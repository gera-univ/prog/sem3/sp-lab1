/*************************************************************************
   LAB 1                                                                

    Edit this file ONLY!

*************************************************************************/

#include "dns.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned int HashtableBucketCount = 4096;

#ifdef _DEBUG
// HashtableBucketCount, HashtableInitialBucketSize, HashtableBucketGrowthAmount can be used
// for tuning hashtable towards reducing the number of realloc calls and optimizing performance time.
int _reallocCount = 0;
#endif

/************************ Hashtable "data type" **************************/

// Static members

const int HashtableInitialBucketSize = 8;
const int HashtableBucketGrowthAmount = 16;

// Data structures

struct Pair {
	char* key;
	unsigned int value;
};

struct Bucket {
	struct Pair** data;
	int size;
	int next;
};

struct Hashtable {
	struct Bucket** buckets;
	int nBuckets;
};

// Methods

/*************************************************************************
 FUNCTION: 
    CreateHashtable()             

 DESCRIPTION:
    Constructs a hash table

 PARAMETERS:
    nBuckets -- number of buckets

 RETURN VALUE:
    A pointer to the newly constructed hashtable structure
************************************************************************/
struct Hashtable* CreateHashtable(int nBuckets);

/*************************************************************************
 FUNCTION: 
    InsertIntoHashtable()             

 DESCRIPTION:
    Inserts the (key, value) pair into hashtable

 PARAMETERS:
    table -- pointer to the hashtable to insert to
    key -- key as char string
    value -- value as unsigned integer

 RETURN VALUE:
    A pointer to the newly constructed hashtable structure

 NOTE:
	If the value already exists in the hashtable, a second one is inserted
************************************************************************/
void InsertIntoHashtable(struct Hashtable* table, const char* key, unsigned int value);

/*************************************************************************
 FUNCTION: 
    FindInHashTable()             

 DESCRIPTION:
    Finds the (key, value) pair in the hashtable

 PARAMETERS:
    table -- pointer to the hashtable to search in
    key -- key as char string

 RETURN VALUE:
    A pointer to the Pair structure if one was found or NULL if not found
************************************************************************/
struct Pair* FindInHashtable(struct Hashtable* table, const char* key);

/*************************************************************************
 FUNCTION: 
    FindInHashTable()             

 DESCRIPTION:
    Frees the memory used by the hashtable

 PARAMETERS:
    table -- pointer to the hashtable to delete
************************************************************************/
void DestroyHashtable(struct Hashtable* table);

/*************************************************************************
 FUNCTION: 
    MakeIP()             

 DESCRIPTION:
    Constructs IP address from four parts

 PARAMETERS:
    Four integers in range 0-255

 RETURN VALUE:
    Value of type IPADDRESS

 NOTE:
    If any of the arguments exceeds the range it is trimmed to 255
************************************************************************/
IPADDRESS MakeIP(unsigned int ip1, unsigned int ip2, unsigned int ip3, unsigned int ip4) {
	return (ip1 & 0xFF) << 24 |
		(ip2 & 0xFF) << 16 |
		(ip3 & 0xFF) << 8 |
		ip4 & 0xFF;
}

struct DNSServer {
	struct Hashtable* hosts;
};

struct DNSServer* getServerByHandle(DNSHandle handle) {
	return (struct DNSServer*)handle;
}

void PrintError(char* message) {
	printf("Error: %s\n", message);
}

DNSHandle InitDNS() {
	struct DNSServer* server = malloc(sizeof(struct DNSServer));
	if (server) {
		server->hosts = CreateHashtable((int)HashtableBucketCount);
		return (DNSHandle)server;
	}
	return INVALID_DNS_HANDLE;
}

void LoadHostsFile(DNSHandle hDNS, const char* hostsFilePath) {
	FILE* hosts_file;
	const errno_t err = fopen_s(&hosts_file, hostsFilePath, "r"); // read only

	if (err == 0 && hosts_file) {
		struct DNSServer* server = getServerByHandle(hDNS);

		char buffer[201] = {0};

		while (!feof(hosts_file)) {
			unsigned int ip1 = 0, ip2 = 0, ip3 = 0, ip4 = 0;
			fgets(buffer, 200, hosts_file);
			fscanf_s(hosts_file, "%d.%d.%d.%d %s", &ip1, &ip2, &ip3, &ip4, buffer, 200);
			const IPADDRESS ip = MakeIP(ip1, ip2, ip3, ip4);
			InsertIntoHashtable(server->hosts, buffer, ip);
		}
	}
}

IPADDRESS DnsLookUp(DNSHandle hDNS, const char* hostName) {
	struct DNSServer* server = getServerByHandle(hDNS);

	struct Pair* p = FindInHashtable(server->hosts, hostName);
	if (p != NULL) {
		return p->value;
	}
	return INVALID_IP_ADDRESS;
}

void DeleteDNSServer(DNSHandle handle) {
	struct DNSServer* server = getServerByHandle(handle);
	DestroyHashtable(server->hosts);
	free(server);
}

void ShutdownDNS(DNSHandle hDNS) {
#ifdef _DEBUG
	printf("realloc count: %d", _reallocCount);
#endif
	DeleteDNSServer(hDNS);
}

unsigned int GetStringHash(const char* charArray) {
	unsigned int result = 0;
	unsigned int nSize = strlen(charArray);
	for (unsigned int i = 0; i < nSize; ++i) {
		result = 31 * result + charArray[i];
	}
	return result;
}

/************************ Hashtable implementation **************************/

unsigned int GetBucketIdInHashtable(const struct Hashtable* table, const char* key) {
	const unsigned int hash = GetStringHash(key);
	return hash % table->nBuckets;
}

void CreateHashtableBucket(struct Hashtable* table, int i) {
	table->buckets[i] = malloc(sizeof(struct Bucket));
	if (table->buckets[i]) {
		table->buckets[i]->size = HashtableInitialBucketSize;
		table->buckets[i]->next = 0;
		table->buckets[i]->data = malloc(sizeof(struct Pair) * HashtableInitialBucketSize);
	}
	else {
		PrintError("mallocing bucket failed");
	}
}

struct Hashtable* CreateHashtable(int nBuckets) {
	struct Hashtable* table = malloc(sizeof(struct Hashtable));
	if (table) {
		table->nBuckets = nBuckets;
		table->buckets = malloc(table->nBuckets * sizeof(struct Bucket*));
		for (int i = 0; i < nBuckets; ++i) {
			CreateHashtableBucket(table, i);
		}
		return table;
	}
	return NULL;
}

void InsertIntoHashtable(struct Hashtable* table, const char* key, unsigned int value) {
	struct Pair* p = malloc(sizeof(struct Pair));
	const unsigned int keySize = strlen(key) + 1;
	if (p) {
		p->key = malloc(keySize);
		if (p->key) {
			strcpy_s(p->key, keySize, key);
			p->value = value;
		}
		else {
			PrintError("mallocing pair key failed");
		}
	}
	else {
		PrintError("mallocing pair failed");
	}
	const unsigned int bucketId = GetBucketIdInHashtable(table, key);
	struct Bucket* b = table->buckets[bucketId];
	if (b->next + 1 >= b->size) {
#ifdef _DEBUG
		++_reallocCount;
#endif
		struct Pair** data = realloc(b->data, sizeof(struct Pair) * (b->size + HashtableBucketGrowthAmount));
		if (!data) {
			free(b->data);
			PrintError("reallocing bucket data failed");
		}
		b->data = data;
		b->size += HashtableBucketGrowthAmount;
	}
	if (b->data)
		b->data[b->next] = p;
	b->next += 1;
}

struct Pair* FindInHashtable(struct Hashtable* table, const char* key) {
	const unsigned int bucketId = GetBucketIdInHashtable(table, key);
	struct Bucket* b = table->buckets[bucketId];
	if (b->size == 0) {
		return NULL;
	}
	for (int i = 0; i < b->next; ++i) {
		if (strcmp(b->data[i]->key, key) == 0)
			return b->data[i];
	}
	return NULL;
}

void DestroyHashtable(struct Hashtable* table) {
	if (table->buckets) {
		for (int i = 0; i < table->nBuckets; ++i) {
			if (table->buckets[i]) {
				for (int j = 0; j < table->buckets[i]->next; ++j) {
					if (table->buckets[i]->data[j])
						free(table->buckets[i]->data[j]->key);
					free(table->buckets[i]->data[j]);
				}
				free(table->buckets[i]);
			}
		}
	}

	free(table);
}
